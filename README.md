# OpenML dataset: Medical-Appointment-No-Shows

https://www.openml.org/d/43439

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
A person makes a doctor appointment, receives all the instructions and no-show. Who to blame? 
If this help you studying or working, please dont forget to upvote :).  Reference to Joni Hoppen and Aquarela Advanced Analytics  Aquarela
Greetings! 
Content
110.527 medical appointments its 14 associated variables (characteristics). The most important one if the patient show-up or no-show to the appointment.  Variable names are self-explanatory, if you have doubts, just let me know! 
scholarship variable means this concept = https://en.wikipedia.org/wiki/Bolsa_FamC3ADlia
14 variables 
Data Dictionary
01 - PatientId

Identification of a patient

02 - AppointmentID

Identification of each appointment

03 - Gender

Male or Female . Female is the greater proportion, woman takes way more care of they health in comparison to man.

04 - DataMarcacaoConsulta

The day of the actuall appointment, when they have to visit the doctor.

05 - DataAgendamento

The day someone called or registered the appointment, this is before appointment of course.

06 - Age

How old is the patient.

07 - Neighbourhood

Where the appointment takes place. 

08 - Scholarship

True of False . Observation, this is a broad topic, consider reading this article https://en.wikipedia.org/wiki/Bolsa_FamC3ADlia 

09 - Hipertension

True or False

10 - Diabetes

True or False

Alcoholism

True or False

Handcap

True or False

SMS_received

1 or more messages sent to the patient.

No-show

True or False. 

Inspiration
What if that possible to predict someone to no-show an appointment?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43439) of an [OpenML dataset](https://www.openml.org/d/43439). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43439/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43439/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43439/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

